'''Custom exceptions classes.'''


class ConfigFileExtensionError(Exception):
    '''Incorrect config file extension (not .ini)'''
    pass


class ConfigFileNotFoundError(FileNotFoundError):
    '''Config file not found, incorrect config path.'''


class PathNotFoundError(NameError):
    '''File path in config.ini not found.'''
    pass


class ServerNotStartedError(ConnectionRefusedError):
    '''Server not started, Restart server.
    
    to restart server run "python3 -m server_script" in bash.
    '''
    pass
