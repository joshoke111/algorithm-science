'''Test client script.

test_main function recieves a iterable of messages as an argument,
it sends the messages as encoded bytes to a server 
already listening for connections.
'''

import sys
sys.path.insert(0,'../')

import socket
from exceptions import ServerNotStartedError

def test_main(messages):
    '''Test client method.
    
     it connects to test port 8084
     '''
    print('Creating Socket Client Connection')
    c_sock=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    print('Client Socket Defined Successfully!\n')
    print(' Client Getting Host Name and Port\n')
    server_address=(socket.gethostname(),8084)
    print('Host Name and Port Recieved Successfuly!\n')
    print('Connecting Client Server to Host Server\n')
    c_sock.connect(server_address)
    print('Connected to Server Successfully!\n')
    try:
        n=0 # Number of processed requests.
        i=0 # break iteration flag
        while True:
            i+=1
            for message in messages:
                print('Sending Data: ',message,'\n')
                try:
                    # Encodes message to send to client
                    c_sock.sendall(message.encode()) 
                except BrokenPipeError:
                    raise BrokenPipeError(
                        'Connection closed by peer(server)'
                        )
                print('Data Sent Successfully!\n')
                data=c_sock.recv(1024).decode()
                print('...waiting for response')
                print('\n RESPONSE RECEIVED: ',data,'\n')
                n+=1 # increment number of proceed requests count.
                print('Number of Request Processed: ',n)
                print('- '*20)
            if i>9000: break
        print('Total Number: ',n)
    finally:
        print('Closing Sockets...\n')
        c_sock.close()
        print('Connection Closed Successful!\n')

if __name__=='__main__':
    try:
        test_main(['Joshua','String','0','10','10;0;1;26',
                   '10;0;1;26;0;9;3;0;','10;0;6;28;0;23;5;0;',
                   '10;0;1;26;0;97;3;45;','3;0;1;28;0;7;5;0;'])
    except ConnectionRefusedError:
        raise ServerNotStartedError(
            'Server is not listening for connections, Restart Server.'
            )