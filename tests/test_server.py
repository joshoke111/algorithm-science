'''Server test script.

Tests for the config file,linux path and server for errors.
'''

import sys
sys.path.insert(0,'../')

import pytest
from server_script import Server
import exceptions


def test_incorrect_config_extension():
        '''
        Test for incorrect config file extension.
        
        Raise Custom 'ConfigFileNameError' Exception,
        if config file path is not .ini
        '''
        with pytest.raises(exceptions.ConfigFileExtensionError) as excinfo:
                Server('../files/config.txt').config_handle()
        assert str(excinfo.value)=='Invalid config file extension.'

def test_correct_config_path():
        '''Test for correct Config File extension.'''

        file=Server('../files/config.ini').config_handle()
        assert type(file) == str,'Test correct file path failed.'


def test_incorrect_config_path():
    '''Test for incorrect config path.
    
    Raises expected custom ConfigFileNotFoundError if invalid config path
    '''

    config_path = 'files/config.ini'
    with pytest.raises(exceptions.ConfigFileNotFoundError) as excinfo:
                Server(config_path).config_handle()
    assert str(excinfo.value) == (config_path+' not a valid file path.')


def test_config_without_linuxpath():
    '''Test for config file without "linuxpath".'''

    with pytest.raises(exceptions.PathNotFoundError) as excinfo:
        Server('../files/config_test.ini').config_handle()
    assert str(excinfo.value) == 'path not found in config file'


def test_no_file_in_path():
    '''Test if file not in path.'''

    file_path=['../files/20k_wrong.txt',
               '../files/missing_wrong.txt',
               '../files/wrong.txt']
    for fp in file_path:
        with pytest.raises(FileNotFoundError) as excinfo:
                Server().text_search('String',fp)
        assert str(excinfo.value) == 'file not found in path '+fp

def test_string_search_():
    '''Test if string  search outputs expected response.
    
        10 sample inputs,their expected and unexpected outputs are used for testing.
    '''
    
    file_path='../files/200k.txt' # config file path

    sample_inputs =(
        '13;0;6;28;0;23;5;0;','13:0:5','23;0;1;16;0;8;5;0;','0',
        '2;0;6;16;0;6;5;0;','String','20;0;23;11;0;22;3;0;','Joshua',
        '20;0;23;11;0;23;5;0;','abcd',)
    expected_outputs =(
        'STRING EXISTS\n','STRING NOT FOUND\n','STRING EXISTS\n',
        'STRING NOT FOUND\n','STRING EXISTS\n','STRING NOT FOUND\n',
        'STRING EXISTS\n','STRING NOT FOUND\n','STRING EXISTS\n',
        'STRING NOT FOUND\n',)
    wrong_outputs =(
        'STRING NOT FOUND\n','STRING EXISTS\n','STRING NOT FOUND\n',
        'STRING EXISTS\n','STRING NOT FOUND\n','STRING EXISTS\n',
        'STRING NOT FOUND\n','STRING EXISTS\n','STRING NOT FOUND\n',
        'STRING EXISTS\n',)

    # Test for expected output
    for input,output in zip(sample_inputs,expected_outputs):
        assert Server().text_search(input,file_path) == output,\
        'Failed Test: '+input+' Expected output: '+output

    # Test for unexpected output
    for input,output in zip(sample_inputs,wrong_outputs):
        assert Server().text_search(input,file_path) != output,\
        'Failed Test: '+input+' Expected output: '+output
