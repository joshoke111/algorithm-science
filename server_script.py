''' A TCP server script.

    script listens for connections from client(s),responds to request.
    The "Server" class wraps every method. 
    run script from command line as "python3 -m server_script"
'''
import sys
import os
from datetime import datetime
import logging
from logging import handlers 

import socket
import threading
from _thread import start_new_thread
import configparser
import unicodedata  # Strip characters such as (\x00) from string.
import mmap


# Import custom file exceptions
from exceptions import ConfigFileExtensionError,PathNotFoundError,ConfigFileNotFoundError

# set up logger
logger=logging.getLogger('server_script_logs')
logger.setLevel(10)
logger.addHandler(logging.StreamHandler(sys.stdout))

# Set lock variable.
print_lock = threading.Lock() 

REREAD_ON_QUERY=True


class Server:
    '''
    Parse file and Implement Thread
    '''

    def __init__(self,config_fname: str='files/config.ini'):
        self.config_fname = config_fname
    
    def config_handle(self) -> str:
        '''Handle parsing of config file.

            returns:
            --------
                file_path: str
        '''
        parser = configparser.ConfigParser()
        if self.config_fname.endswith('ini'): 
            parser.read(self.config_fname)
        else:
            # Raise Custom Exception if file extension is not .ini
            raise ConfigFileExtensionError('Invalid config file extension.')
        # Check if config_fname is a valid file
        if not os.path.isfile(self.config_fname):
            print('A'*10)
            raise ConfigFileNotFoundError(self.config_fname+' not a valid file path.')

        file_path = ''
        for section in parser.sections():
            # Parse sections to extract
            file_path = parser.get(section,'linuxpath',
                                   fallback=None)
            if file_path: 
                break 
        if not file_path:
            # raise a custom exception if "linuxpath" not in config file.
            raise PathNotFoundError('path not found in config file')

        return file_path 
    
    def text_search(self,string: str,file_path: str = None) -> str:
        ''' Open the text file and search for string in text.

            Arguments:
            ----------
            string: str 
                String to search in file.
            file_path: str, optional
                provided to enable direct unit test of the function,

            Returns:
            --------
            str
        '''
        if file_path:
            pass
        else:
            file_path = self.config_handle()
        try:
            with open(file_path,'rb',0) as file:
                # Open file using memory mapping from the mmap library.
                with mmap.mmap(
                        file.fileno(),0,access=mmap.ACCESS_READ) as s:
                    # Append new line character to end of string.
                    string = ''.join([string,'\r\n'])
                    if s.find(string.encode()) != -1:
                        # s.find() returns -1 if string not in s. 
                        return 'STRING EXISTS\n'
        except FileNotFoundError: 
            raise FileNotFoundError('file not found in path '+file_path)

        return 'STRING NOT FOUND\n'
 
    def threaded_process(self,conn) -> None:
        '''
        Implement thread function.
        
        Arguments:
        ----------
        conn: 
            connection object
        '''
        process_time = 0 # Execution time for a client in a thread.
        num_of_request = 0  # Number of request per client in a thread.
        while True: 
            start_process_time = datetime.now() # File search timer
            # Receive data from client and decode
            data = conn.recv(1024).decode()
            if data:
                data = unicodedata.normalize('NFKD',data)
                response = self.text_search(data)
                logger.debug('Search Query: '+data)
                end_process_time = datetime.now() # End string search timer 
                exec_time = (
                    end_process_time-start_process_time
                    ).microseconds/1000

                process_time += exec_time
                num_of_request += 1
                logger.debug(
                    'Execution time: '+str(exec_time)+' milliseconds'+'\n')
                # send encoded response to client
                try:
                    conn.send(response.encode())
                except ConnectionResetError:
                    raise ConnectionResetError(
                        'packet data size received cannot be processed.')
            else:
                # release lock on exit
                print_lock.release()
                break
        if num_of_request > 0: 
            logger.debug(
                '\n\nAverage execution time of {} requests is {:.3f} ms: '
                .format(num_of_request,process_time/num_of_request)
                .upper())
        else:
            print('No Data recieved from client. Closing connection')
        # close connection
        conn.close()

    def main(self):
        ''' 
        Set up the server connection.
        listen for incoming requests,accept requests,
        call the start new thread function.
        '''
        sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        # Get Host Name and Port
        socket_address = (socket.gethostname(),8084)
        sock.bind(socket_address) 
        sock.listen()
        print('Server started')
        print('Active listening for connections!\n')
        while True:
            # Waiting For Connection
            connection,address = sock.accept()

            # Logging info to client and stdout'''
            logger.addHandler(handlers.SocketHandler(address[0],
                              address[1]))
            logger.debug('DEBUG: \n')
            logger.debug('Client host address: '+str(address[0])+'\n')
            logger.debug('Client port: '+str(address[1])+'\n')
            logger.debug(
                'Connection timestamp: '+str(datetime.now()) +'\n')

            # Acquire thread lock and block other client until release.
            print_lock.acquire()
            # Assign client to locked thread.
            start_new_thread(self.threaded_process,(connection,))


if __name__ == '__main__':
    Server().main()
